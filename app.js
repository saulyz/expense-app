import Expenses from "./expenses.js";
import Dashboard from "./dashboard.js";
import UI from "./ui.js";

const ui = new UI();

const expenses = new Expenses();
console.log(expenses);

expenses.addExpense("accomodation", "2019/08/01", 200);
expenses.addExpense("accomodation", "2019/08/02", 20);
expenses.addExpense("accomodation", "2019/07/01", 200);
expenses.addExpense("leisure", "2019/08/03", 15);
expenses.addExpense("tuition", "2019/05/01", 30);
expenses.addExpense("tuition", "2018/05/02", 30);
expenses.addExpense("food", "2019/06/10", 30);
expenses.addExpense("food", "2019/08/27", 10);

const dashboard = new Dashboard(
	document.getElementById("myChart").getContext("2d")
);

console.log(dashboard);

setTimeout(() => {
	expenses.addExpense("transportation", "2019/08/01", 18);
	expenses.addExpense("transportation", "2019/07/01", 8);
	expenses.addExpense("food", "2019/08/20", 10);
	dashboard.update();
}, 2000);
