const categories = [
	{ name: "food", title: "Food", values: [], rgbColorValue: "255, 99, 132" },
	{
		name: "accomodation",
		title: "Accomodation",
		values: [],
		rgbColorValue: "54, 162, 235"
	},
	{
		name: "transportation",
		title: "Transportation",
		values: [],
		rgbColorValue: "255, 206, 86"
	},
	{
		name: "tuition",
		title: "Tuition",
		values: [],
		rgbColorValue: "75, 230, 192"
	},
	{
		name: "leisure",
		title: "Leisure",
		values: [],
		rgbColorValue: "153, 102, 255"
	}
];

export default class Expenses {
	static instance;

	constructor() {
		if (Expenses.instance) {
			return Expenses.instance;
		}

		this.data = {};
		categories.map(item => {
			this.data[item.name] = item;
		});

		Expenses.instance = this;
	}

	getCategories() {
		return categories;
	}

	getCategoryByName(name) {
		return categories.filter(item => item.name === name)[0];
	}

	addExpense(name, date, value) {
		this.data[name].values.push({
			date: date,
			value: value
		});
	}
}
