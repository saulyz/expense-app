// IMPORTANT! Have dependency from material-datepicker, momentjs

import Expenses from "./expenses.js";

export default class UI {
	constructor() {
		this.store = new Expenses();
		this.areas = {
			formArea: document.getElementById("form-area")
		};

		this.setup();
	}

	setup() {
		this.createInputForm();
	}

	createInputForm() {
		let form = document.createElement("form");
		form.setAttribute("id", "input-form");

		let header = document.createElement("h5");
		header.innerText = "Add expenses";
		form.appendChild(header);

		let help = document.createElement("div");
		help.classList.add("help");
		help.style.color = "red";
		form.appendChild(help);

		// all form inputs
		form.appendChild(this.createExpenseCategoryInput());
		form.appendChild(this.createExpenseDateInput());
		form.appendChild(this.createExpenseAmountInput());
		form.appendChild(this.createButton());

		this.areas.formArea.appendChild(form);
		// dependency to createExpenseDateInput()
		let datePicker = new MaterialDatepicker("#expenseDate");

		form.addEventListener("submit", this.inputFormSubmitHandler);
	}

	createExpenseCategoryInput() {
		let categories = this.store.getCategories();
		let input = document.createElement("div");
		input.classList.add(
			"mdl-textfield",
			"mdl-js-textfield",
			"mdl-textfield--floating-label",
			"getmdl-select"
		);
		input.innerHTML = `
      <input
        type="text"
        value=""
        class="mdl-textfield__input"
        id="categoryName"
        readonly
      />
      <input type="hidden" value="" name="categoryName" />
      <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
      <label for="categoryName" class="mdl-textfield__label">
        Expense category
      </label>
      <ul
        for="categoryName"
        class="mdl-menu mdl-menu--bottom-left mdl-js-menu"
      >
      </ul>
    `;
		let list = input.querySelector(".mdl-menu");
		let items = "";
		categories.map(category => {
			items += `<li class="mdl-menu__item" data-val="${category.name}">${category.title}</li>`;
		});
		list.innerHTML = items;
		return input;
	}

	createExpenseDateInput() {
		let input = document.createElement("div");
		input.classList.add(
			"mdl-textfield",
			"mdl-js-textfield",
			"mdl-textfield--floating-label"
		);
		input.innerHTML = `
    <input
      class="mdl-textfield__input"
      type="text"
			id="expenseDate"
			name="expenseDate"
    />
    <label class="mdl-textfield__label" for="expenseDate">
      Date
    </label>
    `;
		return input;
	}

	createExpenseAmountInput() {
		let input = document.createElement("div");
		input.classList.add(
			"mdl-textfield",
			"mdl-js-textfield",
			"mdl-textfield--floating-label"
		);
		input.innerHTML = `
    <input
      class="mdl-textfield__input"
      type="text"
      pattern="-?[0-9]*(\.[0-9]+)?"
			id="expenseAmount"
			name="expenseAmount"
    />
    <label class="mdl-textfield__label" for="expenseAmount">
      Amount
    </label>
    `;
		return input;
	}

	createButton() {
		let button = document.createElement("div");
		button.innerHTML = `
    <button
      class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect"
    >
      Add
    </button>
    `;
		return button;
	}

	inputFormSubmitHandler(event) {
		event.preventDefault();

		let isValid = true;
		let form = this;
		let formHelp = form.querySelector(".help");
		formHelp.innerText = "";
		let formElements = form.elements;
		let formObject = Array.from(formElements)
			.filter(item => {
				if (item.name) return item;
			})
			.map(item => {
				let fieldObject = {
					name: item.name,
					value: item.value
				};
				let validation = new fieldValidation(fieldObject);
				let fieldValid = validation.validate();
				isValid = isValid && fieldValid;
				return fieldObject;
			});

		if (!isValid) {
			form.classList.add("form-invalid");
			formHelp.innerText = "one or more fields are invalid!";
		} else {
			// todo submit
		}
	}
}

const fieldValidationStrategies = {
	categoryName: {
		validate: function() {
			return this.value !== "";
		}
	},
	expenseDate: {
		validate: function() {
			let tomorrow = moment(new Date(), "YYYY/MM/DD").add(1, "minutes");
			let compareDate = moment(this.value, "YYYY/MM/DD");
			return tomorrow > compareDate;
		}
	},
	expenseAmount: {
		validate: function() {
			return parseFloat(this.value) != NaN && parseFloat(this.value) > 0;
		}
	}
};

class fieldValidation {
	constructor(data) {
		this.name = data.name;
		this.value = data.value;
		this.setStrategy(data.name);
	}

	setStrategy(name) {
		this.validate = fieldValidationStrategies[name].validate;
	}
}
