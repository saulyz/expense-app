// IMPORTANT! Have dependency from Chart.js
import Expenses from "./expenses.js";

// external helper function
const summingReducer = (accumulator, currentValue) =>
	accumulator + currentValue.value;

export default class Dashboard {
	constructor(ctx) {
		this.source = new Expenses();
		this.setupType();
		this.setupOptions();
		this.makeDatasets();

		ctx.height = 400;
		this.chart = new Chart(ctx, {
			type: this.type,
			data: this.data,
			options: this.options
		});
	}

	makeDatasets() {
		let periods = this.getPeriods();
		let categories = this.source.getCategories();
		let datasets = categories.map(category =>
			this.getDataset(category.name, periods)
		);

		this.data = {
			labels: periods,
			datasets: datasets
		};
	}

	setupType() {
		this.type = "bar";
	}

	setupOptions() {
		this.options = {
			maintainAspectRatio: false,
			scales: {
				xAxes: [
					{
						stacked: true
					}
				],
				yAxes: [
					{
						stacked: true
					}
				]
			}
		};
	}

	getPeriods() {
		let categories = this.source.getCategories();
		return [
			...new Set(
				categories.flatMap(category =>
					category.values.map(entry => entry.date.substring(0, 7))
				)
			)
		].sort();
	}

	getDataset(categoryName, periods) {
		let data = [];
		let category = this.source.getCategoryByName(categoryName);
		let values = category.values;

		for (let i = 0; i < periods.length; i++) {
			let periodValues = values.filter(
				entry => entry.date.substring(0, 7) === periods[i]
			);
			let sum = periodValues.reduce(summingReducer, 0);
			data.push(sum);
		}

		return {
			label: category.title,
			data: data,
			backgroundColor: `rgba(${category.rgbColorValue}, 0.2)`,
			borderColor: `rgba(${category.rgbColorValue}, 1)`,
			borderWidth: 1
		};
	}

	update() {
		this.makeDatasets();
		this.chart.type = this.type;
		this.chart.options = this.options;
		this.chart.data = this.data;
		this.chart.update();
	}
}
